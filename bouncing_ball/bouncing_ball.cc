/*
 * Bouncing ball - visual bouncing balls on the 32 core starburst(-helix??)
 */

// Includes
#include <helix.h>         // Who knows what this does? :p
#include <render/render.h> // Graphics rendering
#include <stdlib.h>        // Random number gen
#include <time.h>

// Definitions
// Screen defs
#define SIZE_X 640
#define SIZE_Y 480
#define TIME_X 10
#define TIME_Y 10

// Ball defs
#define N_BALLS 90
#define MAX_CORE 32
#define FIFO_SIZE 1
#define BALL_SIZE 30

// Ball speed is max BALL_SPEED_LIMIT per dt at inialization
#define BALL_SPEED_LIMIT 10

#define NANOSECONDS_IN_SECOND 1000000000UL

#define ERREXIT(str)                                                           \
  {                                                                            \
    fprintf(stderr, "Error: " str "\n");                                       \
    exit(1);                                                                   \
  }
#define ERREXIT2(str, ...)                                                     \
  {                                                                            \
    fprintf(stderr, "Error: " str "\n", __VA_ARGS__);                          \
    exit(1);                                                                   \
  }

// Array of available colors in render.h
const int NB_OF_COLORS = 9;
color_t colors[] = {red,   green, blue,   yellow, orange,
                    white, gray,  purple, pink};

// Definition of a ball
typedef struct {
  int proc;      // Processor core
  int x;         // x position
  int y;         // y position
  int dx;        // Movement in x direction per dt
  int dy;        // Movement in y direction per dt
  color_t color; // Color of the ball

  // Communication FIFOs
  CFifo<bool, CFifo<>::w> *next_w;
  CFifo<bool, CFifo<>::r> *start_r;
  CFifo<bool, CFifo<>::w> *ball_w;
  CFifo<bool, CFifo<>::r> *prev_r;
} ball_t;

typedef struct {
  int proc;
  CFifo<bool, CFifo<>::w> *start_w;
  CFifo<bool, CFifo<>::r> *ready_r;
} manager_t;

ball_t balls[N_BALLS];
manager_t manager;

// Arrays containing PIDs and FIFOs
// The last element(s) are for the manager
pid_t pid[N_BALLS + 1];
CFifoPtr<bool> fifo[2 * (N_BALLS + 1)];

// Draw FPS
void draw_time(struct timespec *current, struct timespec *previous) {
  char timestring[30];
  float fps =
      (float)NANOSECONDS_IN_SECOND / (current->tv_nsec - previous->tv_nsec);
  sprintf(timestring, "FPS: %2.3f\n", fps);
  drawstring(TIME_X, TIME_Y, timestring, red);
}

// Thread that starts a draw sequence and blanks the screen.
void *manager_thread(void *arg) {
  struct timespec ts_current;
  struct timespec ts_previous;

  printf("Started manager thread\n");

  // Validate FIFOs
  manager.start_w->validate();
  manager.ready_r->validate();

  while (1) {
    // Blank screen
    fillrect(0, 0, SIZE_X - 1, SIZE_Y - 1, black);

    // Set the first ball to ready
    manager.start_w->push(true);

    // Wait for the last ball
    manager.ready_r->pop();

    // Get the time and draw FPS
    clock_gettime(CLOCK_MONOTONIC, &ts_current);
    draw_time(&ts_current, &ts_previous);
    ts_previous = ts_current;

    // Render/flip buffer
    render_flip_buffer();
  }

  return NULL;
}

int rand_speed() {
  return (rand() % (2 * BALL_SPEED_LIMIT)) - BALL_SPEED_LIMIT;
}

// Initialise all balls with required attributes
void create_balls() {
  for (int i = 0; i < N_BALLS; i++) {
    balls[i].x = rand() % SIZE_X;
    balls[i].y = rand() % SIZE_Y;
    balls[i].dx = rand_speed();
    balls[i].dy = rand_speed();
    balls[i].color = colors[(rand() % NB_OF_COLORS)];
    balls[i].proc = (i + 1) % (MAX_CORE - 1);
  }
}

// Moves a ball
void move_ball(ball_t *ball) {
  // Update x location
  ball->x += ball->dx;

  // Check edge x
  if (ball->x > SIZE_X - 1 || ball->x < 0) {
    ball->dx = -ball->dx;
    ball->x = ball->x + 2 * ball->dx;
  }

  // Update y location
  ball->y += ball->dy;

  // Check edge y
  if (ball->y > SIZE_Y - 1 || ball->y < 0) {
    ball->dy = -ball->dy;
    ball->y = ball->y + 2 * ball->dy + (ball->dy / abs(ball->dy));
  }
}

// Draw a circle
void draw_ball(int x0, int y0, int radius, int color) {
  int x = radius;
  int y = 0;
  int err = 0;

  while (x >= y) {
    drawpixel(x0 + x, y0 + y, color);
    drawpixel(x0 + y, y0 + x, color);
    drawpixel(x0 - y, y0 + x, color);
    drawpixel(x0 - x, y0 + y, color);
    drawpixel(x0 - x, y0 - y, color);
    drawpixel(x0 - y, y0 - x, color);
    drawpixel(x0 + y, y0 - x, color);
    drawpixel(x0 + x, y0 - y, color);

    y += 1;
    if (err <= 0) {
      err += 2 * y + 1;
    }
    if (err > 0) {
      x -= 1;
      err -= 2 * x + 1;
    }
  }
}

// Ball thread
void *ball_thread(void *arg) {
  ball_t *ball;

  ball = (ball_t *)arg;

  // Validate FIFOs
  // ball->start_r->validate();
  // ball->next_w->validate();
  // ball->prev_r->validate();
  // ball->ball_w->validate();

  while (1) {
    // Wait for signal
    ball->start_r->pop();

    // Signal next ball
    if (ball->next_w != NULL)
      ball->next_w->push(true);

    // Move ball without cache
    DisableDCache();
    move_ball(ball);
    EnableDCache();

    // Draw ball & flush cache
    draw_ball(ball->x, ball->y, BALL_SIZE / 2, ball->color);
    FlushDCache();

    // Wait for previous ball
    if (ball->prev_r != NULL)
      ball->prev_r->pop();

    // Signal ready
    ball->ball_w->push(true);
  }

  return NULL;
}

// Creates and starts a process with PID index i on core proc
void create_process(int i, int proc, void *(*main)(void *), void *arg) {
  if (int e = CreateProcess(pid[i], main, arg, PROC_DEFAULT_TIMESLICE,
                            PROC_DEFAULT_STACK, proc)) {
    ERREXIT2("Process creation failed for %i: %i", i, e);
  }

  if (int e = SetProcessFlags(pid[i], PROC_FLAG_JOINABLE, proc)) {
    ERREXIT2("While setting process flags for %i: %i", i, e);
  }

  if (int e = StartProcess(pid[i], proc)) {
    ERREXIT2("Could not start process %i: %i", i, e);
  }
}

// Wait for a process to exit
void wait_process(int i, int proc) {
  if (int e = WaitProcess(pid[i], NULL, proc)) {
    ERREXIT2("Waiting on pid %i: %i\n", i, e);
  }
}

// Creates fifos with FIFO index i between source and destination cores
void create_fifos(int i, int source_proc, CFifo<bool, CFifo<>::r> *&source_r,
                  CFifo<bool, CFifo<>::w> *&source_w, int dest_proc,
                  CFifo<bool, CFifo<>::r> *&dest_r,
                  CFifo<bool, CFifo<>::w> *&dest_w) {

  // Create start signal FIFO from source to destination
  fifo[2 * i] =
      CFifo<bool>::Create(source_proc, source_w, dest_proc, dest_r, FIFO_SIZE);

  // Create ready signal FIFO from destination to source
  fifo[(2 * i) + 1] =
      CFifo<bool>::Create(dest_proc, dest_w, source_proc, source_r, FIFO_SIZE);

  // Check for errors
  if (!fifo[2 * i].valid() || !fifo[(2 * i) + 1].valid()) {
    ERREXIT("Error creating buffer");
  }
}

// Main
int main(int argc, char **argv) {
  // Initialize random number gen with current time
  srand(time(NULL));

  // Initialize balls
  create_balls();

  // Initialize display
  if (render_init(1) != RENDER_OK) {
    printf("Error: init display!\n");
    return 0;
  }

  // Create FIFOS
  printf("Initializing FIFOs\n");
  for (int i = 0; i < N_BALLS - 1; i++) {
    ball_t *curball = &(balls[i]);      // Pointer to the current ball
    ball_t *nextball = &(balls[i + 1]); // Pointer to the next ball

    // Create start and signal FIFOs between current and next ball
    create_fifos(i, curball->proc, curball->prev_r, curball->next_w,
                 nextball->proc, nextball->start_r, nextball->ball_w);
  }

  // Create manager FIFOs
  printf("Creating manager FIFOs");
  manager.proc = 0;

  // Create start and signal FIFOs between manager and first ball
  create_fifos(N_BALLS, manager.proc, manager.ready_r, manager.start_w,
               balls[0].proc, balls[0].start_r, balls[0].ball_w);

  // Create manager
  printf("Creating and starting process manager\n");
  create_process(N_BALLS, manager.proc, manager_thread, NULL);

  // Create processes
  printf("Creating and starting ball processes\n");
  for (int i = 0; i < N_BALLS; i++) {
    create_process(i, balls[i].proc, ball_thread, &(balls[i]));
  }

  // Wait for manager to exit
  wait_process(N_BALLS, manager.proc);

  // Wait for balls to exit
  for (int i = 0; i < N_BALLS; i++) {
    wait_process(i, balls[i].proc);
  }
}
