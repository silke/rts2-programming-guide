#include <helix.h>

#define NUMPROCS 32

// Write ends of FIFOs
CFifo<bool, CFifo<>::w> *wr[NUMPROCS];

// Read end point FIFOs
CFifo<bool, CFifo<>::r> *rd[NUMPROCS];

#define ERREXIT(str)                                                           \
  {                                                                            \
    fprintf(stderr, "Error: " str "\n");                                       \
    exit(1);                                                                   \
  }
#define ERREXIT2(str, ...)                                                     \
  {                                                                            \
    fprintf(stderr, "Error: " str "\n", __VA_ARGS__);                          \
    exit(1);                                                                   \
  }

void *banner(void *arg) {
  int n, p;
  CFifo<bool, CFifo<>::w> *w;
  CFifo<bool, CFifo<>::r> *r;

  n = *((int *)arg);                 // Number of thread
  p = (n + NUMPROCS - 1) % NUMPROCS; // Number of previous thread
  w = wr[n];                         // Write FIFO (current)
  r = rd[p];                         // Read FIFO (previous)

  // Validate FIFOS
  w->validate();
  r->validate();

  // Startup
  if (n == 0)
    w->push(true);

  // Show startup of thread
  printf("Loop for banner %i start (r: %i, w: %i)\n", n,
         (n + NUMPROCS - 1) % NUMPROCS, n);

  // Display after the previous process
  while (1) {
    r->pop();
    printf("Banner %i\n", n);
    w->push(true);
  }

  return NULL;
}

int main(int argc, char **argv) {
  pid_t pid[NUMPROCS];
  CFifoPtr<bool> fifo[NUMPROCS];

  // Create FIFOS
  for (int i = 0; i < NUMPROCS; i++) {
    fifo[i] = CFifo<bool>::Create(i, wr[i], (i + 1) % NUMPROCS, rd[i], 2);
    if (!fifo[i].valid())
      ERREXIT("Error creating buffer");
  }

  // Create processes
  for (int i = 0; i < NUMPROCS; i++) {
    // Allocate arguments
    void *arg = malloc(sizeof(int));
    *((int *)arg) = i;

    // Create process
    if (int e = CreateProcess(pid[i], banner, arg, PROC_DEFAULT_TIMESLICE,
                              PROC_DEFAULT_STACK, i)) {
      ERREXIT2("Process creation failed: %i", e);
    }
  }

  // Set process flags
  for (int i = 0; i < NUMPROCS; i++) {
    if (int e = SetProcessFlags(pid[i], PROC_FLAG_JOINABLE, i))
      ERREXIT2("While setting process flags: %i", e);
  }

  // Start processes
  for (int i = 0; i < NUMPROCS; i++) {
    printf("Starting %i\n", i);
    if (int e = StartProcess(pid[i], i))
      ERREXIT2("Could not start process: %i", e);
  }

  // FIFOs are destroyed when the pointers goes out of scope
  for (int i = 0; i < NUMPROCS; i++) {
    if (int e = WaitProcess(pid[i], NULL, i))
      ERREXIT2("Waiting on pid %i@%i: %i\n", pid[i], i, e);
  }

  return 0;
}
