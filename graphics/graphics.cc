// Rendering library demo
#include <helix.h>
#include <render/render.h>
#include <stdio.h>
#include <time.h>

// Display size
#define SIZE_X 640
#define SIZE_Y 480

// Tile size
#define TILE_X (SIZE_X / 32)
#define TILE_Y (SIZE_Y / 24)

#define TIME_X 500
#define TIME_Y 30

#define NANOSECONDS_IN_SECOND 1000000000UL

// Draw tiles
void draw_tile(int pos_x, int pos_y) {
  // Guard against drawing outside screen
  int x_start = (pos_x < 0 ? 0 : pos_x);
  int y_start = (pos_y < 0 ? 0 : pos_y);

  // Calculate end coordinates of the tiles
  int x_end = pos_x + TILE_X - 1;
  int y_end = pos_y + TILE_Y - 1;
  // Guard end coordinates against drawing outside screen
  x_end = (x_end > SIZE_X - 1 ? SIZE_X - 1 : x_end);
  y_end = (y_end > SIZE_Y - 1 ? SIZE_Y - 1 : y_end);

  // If the tile is not visible anyway
  if (x_start > SIZE_X || y_start > SIZE_Y || x_end < 0 || y_end < 0) {
    return;
  }

  // Draw the tile
  fillrect(x_start, y_start, x_end, y_end, white);
}

void draw_frame(int x_scroll, int y_scroll) {
  bool draw;
  // Draw black background
  fillrect(0, 0, SIZE_X - 1, SIZE_Y - 1, black);

  // Draw set of rectangles
  for (int x_tile = 0; x_tile < SIZE_X + 2 * TILE_X; x_tile += TILE_X) {
    // Determine if this column should start with a black or white square
    if ((x_tile / TILE_X) % 2) {
      draw = false;
    } else {
      draw = true;
    }
    for (int y_tile = 0; y_tile < SIZE_Y + 2 * TILE_Y; y_tile += TILE_Y) {
      if (draw) {
        draw_tile(x_tile - x_scroll, y_tile - y_scroll);
        draw = false;
      } else {
        draw = true;
      }
    }
  }
}

void draw_time(struct timespec *current, struct timespec *previous) {
  char timestring[30];
  float fps = NANOSECONDS_IN_SECOND / (current->tv_nsec - previous->tv_nsec);
  sprintf(timestring, "FPS: %2.3f\n", fps);
  drawstring(TIME_X, TIME_Y, timestring, red);
}

int main(int argc, char **argv) {
  struct timespec ts_current;
  struct timespec ts_previous;
  int x_scroll = 0;
  int y_scroll = 0;
  printf("Starting drawing demo\n");

  // Initialize display
  if (render_init(1) != RENDER_OK) {
    printf("Error: init display!\n");
    return 0;
  }

  // Fill only a part of the screen
  while (1) {
    draw_frame(x_scroll, y_scroll);

    // Increment and reset scroll coordinates
    x_scroll++;
    y_scroll++;
    if (x_scroll >= 2 * TILE_X) {
      x_scroll = 0;
    }
    if (y_scroll >= 2 * TILE_Y) {
      y_scroll = 0;
    }

    // Get the current time
    clock_gettime(CLOCK_MONOTONIC, &ts_current);
    // Draw FPS on the screen
    draw_time(&ts_current, &ts_previous);

    // Flip render buffer
    render_flip_buffer();
    ts_previous = ts_current;
  }
  printf("Done\n");
}
