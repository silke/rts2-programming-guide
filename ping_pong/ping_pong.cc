// Sample to start 3 threads on 3 different cores

#include <helix.h>
#include <time.h>
// Dirty exit routine where processes on other cores keep running

#define ERREXIT(s)                                                             \
  {                                                                            \
    perror(s);                                                                 \
    exit(1);                                                                   \
  }

void *t1(void *arg) {
  while (1) {
    usleep(5000);
    printf("This is thread 1\n");
    usleep(5000);
  }
  return NULL;
}

void *t2(void *arg) {
  while (1) {
    usleep(10000);
    printf("Perchance this might be thread 2\n");
  }
  return NULL;
}

int main(int argc, char **argv) {
  int pid[2];
  // Create 2 processes on 2 specific cores
  if (0 != CreateProcess(pid[0], t1, NULL, PROC_DEFAULT_TIMESLICE,
                         PROC_DEFAULT_STACK, 1))
    ERREXIT("Error starting t1");
  if (0 != CreateProcess(pid[1], t2, NULL, PROC_DEFAULT_TIMESLICE,
                         PROC_DEFAULT_STACK, 2)) {
    ERREXIT("Error starting t2");
  }

  // Make both processes joinable: this means we can wait for them to terminate
  if (0 != SetProcessFlags(pid[0], PROC_FLAG_JOINABLE, 1)) {
    ERREXIT("Error making t1 joinable");
  }
  if (0 != SetProcessFlags(pid[1], PROC_FLAG_JOINABLE, 2)) {
    ERREXIT("Error making t2 joinable");
  }

  // Start both threads
  if (0 != StartProcess(pid[0], 1))
    ERREXIT("Starting t1");
  if (0 != StartProcess(pid[1], 2))
    ERREXIT("Starting t2");

  // Wait for both threads to end
  if (0 != WaitProcess(pid[0], NULL, 1))
    ERREXIT("Joining t1");
  if (0 != WaitProcess(pid[1], NULL, 2))
    ERREXIT("Joining t2");
}
